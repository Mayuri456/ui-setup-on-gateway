# STEP 1
1)In react project add docker file .
2)create docker image using following 
sudo docker build -t delhi_metro_ui:v1.0 .

# STEP 2
1)Install Node.js 16 and npm on Raspberry Pi using following command
curl -sSL https://deb.nodesource.com/setup_16.x | sudo bash -
sudo apt install -y nodejs
node --version
npm --version
sudo npm install --global serve

# STEP 3
1)create project folder in pi (ex kochi_metro_ui)
2)copy build folder using folloing command
scp -r build/ pi@172.16.7.30:/home/pi/kochi-metro-ui

# STEP 4
1)create .service file into /etc/systemd/system/  folder
using following command
sudo cp kochi-metro-ui.service /etc/systemd/system/
then do follow following command

sudo systemctl enable kochi-metro-ui.service
sudo systemctl start kochi-metro-ui.service
sudo systemctl status kochi-metro-ui.service

# STEP 5
1)add chromium file using following   command

mkdir /home/pi/.config/autostart
cd /home/pi/.config/autostart
cp /home/pi/iam-gateway-setup/chromium.desktop /home/pi/.config/autostart/chromium.desktop

# #############################################################################################################################


# service file format
User=pi
Group=pi
WorkingDirectory=/home/pi/project folder
ExecStart= serve -p 2902 -s /home/pi/project folder/build
OR
ExecStart= python3 -m http.server 2902 /home/pi/project folder/build
[Install]
WantedBy=default.target

# chromium file format
[Desktop Entry]
Type=Application
Exec=/usr/bin/chromium-browser --noerrdialogs --check-for-update-interval=31536000 --disable-infobars --disable-session-crashed-bubble --kiosk --app=http://localhost:2902
Hidden=false
  
  
# Docker file format 
 
# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM tiangolo/node-frontend:10 as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build
CMD ["npm" , "start"]
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:latest
COPY --from=build-stage /app/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf
# ################################################################################################################################
# React stable version
sudo npm install -g n
sudo n stable
## Provide net to gateway
curl -X POST http://192.168.100.1:8090/login.xml --data-urlencode "mode=191" --data-urlencode "username=nihar zanwar" --data-urlencode "password=Kiam@12345" --data-urlencode "a=1633529001622" --data-urlencode "producttype=0"

